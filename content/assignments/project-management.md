+++
title = "Week 01 : Project Management"
description = "Use Git and GitLab to manage files in my repository"
+++

## Shift between laptop and Remote website
When I first touched GitLab and created my first website, I got lost in different types of web documents and windows. I entered the GitLab world until I realized that I needed to work in different places: Local ( laptop ) and Remote ( website ).

### Tools for building Website

#### Local/laptop (here I use macOS)
Terminal
Visual Studio Code:
A software that can create web documents and upload them to the GitLab repository.
Its window can be splitter the window into 2 working areas: 
        sidebar (web structure and manage the folders and files)
        web documents
        terminal (install, execute, test)


#### Remote / Website - Online resource and Tutorial and guidelines
GitLab - GitLab repository
Home brew
Hugo

---
#### Git Command Line

<code>git init</code>
Initialize a repository
**git checkout [filename]** shift to the branch
**git log** check all the commits
**git status** check if there are files

---


**STEP 01: Create new hugo project**

mkdir .temp
mv ./* .temp/
hugo new site ./

if there is an error
hugo new site./ --force


![image](./create.temp1.jpg)


**STEP 02: Move all the files to the hidden directory- .temp**


![image](./check.temp2.jpg)




**STEP 03: Make a new skeleton**

![image](./move_files_to.temp3.jpg)




A simple skeleton is easier to manage, so delete some directories:Archetypes, Data,and theme.

![image](./new_skeleton4.jpg)

![image](./simple_skeleton5.jpg)




**STEP 04: Edit "config.toml" and change the address and title**
<title>{{ .Site.Title }}</title>

![image](./modify_address6.jpg)




**STEP 05: Move "index.html" to the "layouts" folder**
<p> {{ .Site.Params.Description }}</p>

![image](./move_index.html7.jpg)

![image](./move_index.html8.jpg)

**STEP 06: Test the Hugo server**



**STEP 07: Do the main layouts**
Make the title variable

![image](./title_variable10.jpg)




**STEP 08: Go to "Config.toml" and add Params**

![image](./add_params11.jpg)




**Remenber go to "index.html" and add params as well**

![image](./add_params12.jpg)




**STEP 09: Create a navigation.html**

![image](./create_navigation12.png)




**STEP 10: Move the "README.md" to the Root**

![image](./readme.md_to_root14.jpg)




**STEP 11: Remove the rest of files in .temp**
git status
git add .gitlab-ci.yml
git commit -m 'Remove old gitlab-ci.yml'

![image](./remove_rest_files15.jpg)




**Commit**

![image](./commit18.jpg)




### Problems I met and solved

**Cannot see the modified web page.**

Remember to save “each” page modified before testing the web pages.
It is simple and basic, but it is really important.
( I stayed overnight to try and try until I found I forgot to save the document I modified.)


**Error, Cannot test web pages.**

(1) Type “hugo server” in terminal, it showed error.
It could mean I am not in the right position to run the test.
Type “cd ..” To go back to the origin place.
And type “hugo server”


(2) Close the terminal and Open it again.
Then the restarted Terminal will bring me back to the place where I can test the website. 

**Use Hugo as the generator, website cannot show the images.**
 In my case, I put images at static (folder), and create “final-project.md (file)” under “content (folder)”. The website could not show the images.

content (folder)
|- final-project.md

static (folder)
	|— image.jpeg

![image](/static/image.jpeg)

**How does it work?**
I create “final-project.md(file)” and “final-project (folder)” under “content (folder)”. And move the image to “final-project (folder)”. And type the shortcode on final-project.md(file)”.

content (folder)
|— final-project.md
|— final-project (folder)
	|— image.jpeg


![image](./image.jpeg)


