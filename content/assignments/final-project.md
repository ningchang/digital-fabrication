+++
title = "Final Project"
description = "Final"
+++

**Under the blanket**

Do you remember a chapter of The Little Prince talking about the animals under a hat? Have you seen your pet sleep under your blanket? Does your little brother hide under a quilt? 

![image1](./little_prince.jpeg)

This project aims to make a series of installation that looks like animals or something under the blanket. To imitate a living creature, I would like to design an electronic device to make this stuff breathe. Yet, I plan to make its appearance like a pet under the blanket, which looks soft and makes people want to pat it and to interact with it.


![image2](./sketches39.png)
![image3](./sketches40.png)
![image4](./sketches41.png)


