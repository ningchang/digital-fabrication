+++
title = "About page"
description = "About"
+++

# Hey!

![image](./IMG_2439.jpg)


Hi, my name is Ya-Ning Chang, just call me Ning. I am from Taiwan. I am a master student in Contemporary Design at Aalto University. With my previous science and design background, I want to integrate what I learned and collaborate with people from different areas.

The motivation for joining Fablab is because these electronics things are totally new to me. Though I am really into different materials, I haven’t tried electronic materials to develop my design. That was why I decided to join the tribe of FabLab. Plus, I plan to explore bio-based material approaches for sustainable design. It is most likely to bring my ideas into reality through these techniques, like 3D printing. 
        